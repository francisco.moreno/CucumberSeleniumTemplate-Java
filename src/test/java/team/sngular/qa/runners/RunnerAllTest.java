package team.sngular.qa.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"team.sngular.qa.steps"},
        features = ".",
        format = {"pretty",
                "html:target/cucumber",
                "json:target/cucumber.json",
                "junit:target/cucumber.xml"},
        tags = {"@complete"}
)
public class RunnerAllTest {
}
