package team.sngular.qa.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.EbayHomePage;
import pageObjects.ListItemsPage;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;

public class BusquedaElementosSteps {
    WebDriver driver;
    EbayHomePage homePage;

    public BusquedaElementosSteps() {
        driver = Hooks.driver;
        homePage = new EbayHomePage(driver);
    }

    @When("^Busco el item \"([^\"]*)\"$")
    public void buscoElItem(String arg0) throws Throwable {
        homePage.searchItem(arg0);
    }

    @Then("^los elementos encontrados contienen \"([^\"]*)\"$")
    public void losElementosEncontradosContienen(String arg0) throws Throwable {
        ListItemsPage listItemsPage = new ListItemsPage(driver);
        List<WebElement> resultados = listItemsPage.getResults();
        for (WebElement item : resultados) {
            String nombreArticulo = item.findElement(By.tagName("a")).getText().toLowerCase();
            assertThat("El artiuclo no coincide con el buscado", nombreArticulo, containsString(arg0.toLowerCase()));
        }
    }
}
