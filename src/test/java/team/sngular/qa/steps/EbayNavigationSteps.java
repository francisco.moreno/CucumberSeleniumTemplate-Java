package team.sngular.qa.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.core.IsNot;
import org.openqa.selenium.WebDriver;
import pageObjects.EbayElectronicsPage;
import pageObjects.EbayHomePage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

public class EbayNavigationSteps {
    WebDriver driver;
    EbayHomePage homePage;
    EbayElectronicsPage electronicsPage;

    public EbayNavigationSteps() {
        driver = Hooks.driver;
        homePage = new EbayHomePage(driver);
        electronicsPage = new EbayElectronicsPage(driver);
    }

    @When("^I navigate to Electronics section$")
    public void iNavigateToElectronicsSection() throws Throwable {
        homePage.goToElectronics();
    }

    @Then("^I see electronics related items$")
    public void iSeeElectronicsRelatedItems() throws Throwable {
        String currentSection = electronicsPage.getSectionName();
        assertThat("No estamos en la sección de electrónica", currentSection, equalTo("Electronics"));
    }

    @Then("^I dont see motor item$")
    public void iSeeDontSeeMotorItem() throws Throwable {
        String currentSection = electronicsPage.getSectionName();
        assertThat("No estamos en la sección de electrónica", currentSection, not("Motors"));
    }
}
