package team.sngular.qa.steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import managers.FileReaderManager;
import managers.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class Hooks {

    public static WebDriver driver;
    managers.WebDriverManager driverManager;

    @Before
    public void openBrowser() throws MalformedURLException {
        System.out.println("Called openBrowser");
        driverManager = new WebDriverManager();
        try {
            driver = driverManager.getDriver();
        } catch (IOException e) {
            fail("Error al abrir el navegador: " + e.getMessage());
            e.printStackTrace();
        }
        String url = FileReaderManager.getInstance().getConfigReader().getApplicationUrl();
        driver.navigate().to(url);
    }

    @After
    public void embedScreenshot(Scenario scenario) {
        if (scenario.isFailed()) {
            try {
                scenario.write("Current Page URL is " + driver.getCurrentUrl());
                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
                String pathName = "./screenshots/" + scenario.getName() + ".png";
                FileUtils.writeByteArrayToFile(new File(pathName), screenshot);

            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        driverManager.closeDriver();
    }
}
