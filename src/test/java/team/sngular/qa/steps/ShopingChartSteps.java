package team.sngular.qa.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import pageObjects.EbayElectronicsPage;
import pageObjects.EbayHomePage;
import pageObjects.ItemPage;
import pageObjects.ListItemsPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class ShopingChartSteps {
    WebDriver driver;
    EbayHomePage homePage;

    public ShopingChartSteps() {
        driver = Hooks.driver;
        homePage = new EbayHomePage(driver);
    }

    @Given("^Estoy en la página principal$")
    public void estoyEnLaPáginaPrincipal() throws Throwable {
        //Nada que hacer
    }

    @And("^el carro de la compra está vacío$")
    public void elCarroDeLaCompraEstáVacío() throws Throwable {
        boolean carroTieneElementos = homePage.checkShopingChart();
        assertThat("El carro no está vacío", carroTieneElementos, is(false));
    }

    @When("^Añado un \"([^\"]*)\" al carrito$")
    public void añadoUnAlCarrito(String arg0) throws Throwable {
        homePage.searchItem(arg0);
        ListItemsPage resultados = new ListItemsPage(driver);
        resultados.goToResultado(0);
        ItemPage itemPage = new ItemPage(driver);
        itemPage.addItemToChart();
    }

    @Then("^En el carrito hay un elemento nuevo$")
    public void enElCarritoHayUnElementoNuevo() throws Throwable {
        boolean carroTieneElementos = homePage.checkShopingChart();
        int countElements = homePage.getShopingChartCount();
        assertThat("El carro no está vacío", carroTieneElementos, is(true));
        assertThat("El carro debería contener un elemento", countElements, is(1));
    }
}
