@complete
Feature: Ebay navigation example

  Scenario: Navigate to Electronics
    When I navigate to Electronics section
    Then I see electronics related items

  Scenario: Check Electronics
    When I navigate to Electronics section
    Then I dont see motor item