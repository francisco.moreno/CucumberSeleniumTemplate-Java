@complete
Feature: Añadir item al carro de la compra
  Como consumidor
  Quiero que al añadir un item al carro de la compra
  Para que quede almacenado y pueda comprarlo posteriormente

  Scenario:  Al añadir un elemento éste debe quedar almancenado en el carrito
    Given Estoy en la página principal
    And el carro de la compra está vacío
    When Añado un "Iphone X Black" al carrito
    Then En el carrito hay un elemento nuevo
