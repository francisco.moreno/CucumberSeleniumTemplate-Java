@complete
Feature:Búsqueda de elementos
  Como consumidor
  Quiero poder buscar elementos
  Para que aparezcan únicamente los items en los que estoy interesado

  Scenario Outline: Busquedas de elementos
    Given Estoy en la página principal
    When Busco el item "<item>"
    Then los elementos encontrados contienen "<termino>"
    Examples:
      | item              | termino |
      | iphone X          | iphone  |
      | samsumng galaxy 8 | samsung |
      | iphone 6          | iphone  |