package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EbayHomePage extends BasePageObject {

    @FindBy(linkText = "Electronics")
    public WebElement electronicsLink;

    @FindBy(id = "gh-ac")
    public WebElement searchInput;
    @FindBy(id = "gh-btn")
    public WebElement searchBtn;

    @FindBy(id = "gh-cart-i")
    public WebElement shopingChartIcon;

    @FindBy(id = "gh-cart-n")
    public WebElement shopingChartElements;


    public EbayHomePage(WebDriver driver) {
        super.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void goToElectronics() {
        electronicsLink.click();
    }

    public void searchItem(String item) {
        searchInput.sendKeys(item);
        searchBtn.click();
    }

    public boolean checkShopingChart() {
        return isElementPresent(shopingChartElements, 5);
    }

    public int getShopingChartCount() {
        waitForVisibility(shopingChartElements);
        return Integer.parseInt(shopingChartElements.getText());
    }
}
