package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ItemPage extends BasePageObject {
    @FindBy(partialLinkText = "Add to cart")
    public WebElement addToChartBtn;

    @FindBy(css = ".clzBtn.viicon-close")
    public WebElement closeChartPopUp;

    public ItemPage(WebDriver driver) {
        super.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void addItemToChart() {
        waitForElementClickable(addToChartBtn);
        addToChartBtn.click();
        if (isElementPresent(closeChartPopUp, 5))
            closeChartPopUp.click();
    }
}
