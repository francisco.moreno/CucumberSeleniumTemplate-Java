package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class ListItemsPage extends BasePageObject {
    @FindBy(css = "#ListViewInner > .sresult > .lvtitle")
    public List<WebElement> results;

    public ListItemsPage(WebDriver driver) {
        super.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public List<WebElement> getResults() {
        waitForVisibilityofAllElements(results, 10);
        return results;
    }

    public void goToResultado(int posicion) {
        List<WebElement> resultados = getResults();
        if (results.size() > posicion) {
            results.get(posicion).findElement(By.tagName("a")).click();
        }
    }
}
