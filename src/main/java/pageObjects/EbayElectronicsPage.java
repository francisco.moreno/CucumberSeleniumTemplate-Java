package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EbayElectronicsPage extends BasePageObject {
    @FindBy(css = ".b-pageheader__text")
    WebElement sectionName;

    public EbayElectronicsPage(WebDriver driver) {
        super.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getSectionName() {
        waitForVisibility(sectionName);
        return sectionName.getText();
    }
}
